Sans base de données :

Supposons que vous souhaitez afficher une liste d'utilisateurs dynamique dans votre template Django sans utiliser de base de données. Vous pouvez simplement fournir ces données directement depuis votre vue.

Voici un exemple de vue qui fournit une liste d'utilisateurs :

python

from django.shortcuts import render

def home(request):
    users = ["Alice", "Bob", "Charlie"]
    context = {"users": users}
    return render(request, "base/home.html", context)
Dans cet exemple, nous avons une liste statique d'utilisateurs stockée dans la variable users. Cette liste est ensuite passée au template sous la clé "users" dans le contexte.

Vous pouvez ensuite accéder à ces utilisateurs dans votre template comme ceci :

html

<ul>
{% for user in users %}
    <li>{{ user }}</li>
{% endfor %}
</ul>
Cela affichera chaque utilisateur dans la liste sous forme d'élément de liste <li>.

Avec base de données :

Pour gérer des données dynamiques à partir d'une base de données, vous pouvez utiliser les modèles Django pour interagir avec la base de données et extraire les données nécessaires.

Supposons que vous avez un modèle User qui représente les utilisateurs dans votre base de données.

Voici un exemple de vue qui extrait les utilisateurs de la base de données et les passe au template :

python

from django.shortcuts import render
from .models import User

def home(request):
    users = User.objects.all()
    context = {"users": users}
    return render(request, "base/home.html", context)
Dans cet exemple, User.objects.all() extrait tous les utilisateurs de la base de données. Ces utilisateurs sont ensuite passés au template de la même manière que dans l'exemple précédent.

Dans votre template, vous pouvez accéder aux propriétés de chaque utilisateur de la même manière qu'auparavant :

html

<ul>
{% for user in users %}
    <li>{{ user.username }}</li>
{% endfor %}
</ul>
Assurez-vous d'avoir créé le modèle User dans votre application Django et que la base de données est configurée correctement dans votre projet.